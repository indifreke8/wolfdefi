---
title: "TreeFund"
date: 2020-02-10T19:51:14-07:00
draft: false
images: ["/img/TreeFund-UI-1-600.png"]
tags:
  - Ethereum
  - Blockchain
  - solidity
  - trustfund
  - DApp
---

{{< image src="/img/TreeFund-UI-1-600.png" alt="TreeFund-UI" position="left" style="border-radius: 4px;" >}}

#### 3/2020 -- [TreeFund on Ropsten](/posts/2020/03/treefund-on-ropsten/)

#### 2/2020 -- [Project Liftoff](/posts/2020/02/introducing-treefund)

#### -->[Code Repository](https://gitlab.com/dentino/treefund)
