---
title: "Ethereum Hd Sweeper"
date: 2020-03-15T15:45:19-06:00
draft: false
toc: false
images: ["/img/ethereum-hd.png"]
tags:
  - Ethereum
  - Blockchain
  - HD-Wallet
  - Nodejs
---

{{< image src="/img/ethereum-hd-2.png" alt="Ethereum Logo" position="center" style="border-radius: 0px;" width="75" height="75">}}

## Purpose

Given an HD xPriv key, this script will iterate through a defined number of hot wallet addresses checking for a balance on each account and sending any coins found to a single cold wallet address. Can use Infura, Etherscan or custom/local node.

[Please see project readme for more info on how to run this tool](https://gitlab.com/dentino/ethereum-hd-sweeper)
