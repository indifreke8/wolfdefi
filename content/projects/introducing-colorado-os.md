---
title: "Introducing Colorado OS"
date: 2021-05-31T01:56:06-07:00
draft: false
toc: false
images: ["/img/colorado-os-NFT_SIDE_A.png", "/img/colorado-os-NFT_SIDE_B.png", "/img/colorado-os-nft-1.png", "/img/colorado-os-nft-2.png", "/img/colorado-os-nft-3.png"]
tags:
  - ethereum
  - colorado-os
  - gaming
  - licensee
  - EIP712
  - colorado-jam
  - ETHDenver
---
{{< image src="/img/colorado-os-nft-1.png" alt="NFT ART 1" position="center">}}

At ETHDenver 2021 and ColoradoJam we built a proof of concept for a digital licensing system for the state of Colorado. We use Ethereum contracts for the license registry and super cool NFTs for state parks passes and gaming licenses. For extra fun, we made the parks passes DeFi charged allowing parks goers to donate funds into an interest bearing pool when purchasing a pass.   

We are excited to have won two different bounties at ETHDenver 2021 with Colorado OS. First, we took one of the five bounties on the Impact track for addressing UN Sustainability Goal (SDG). Our second bounty was taking 1st place on the Colorado Jam track for Digital Licensing solutions. 

We would love to keep developing Colorado OS as an open source example of how state licensing processes (and much more) could be efficiently and securely handled with Web3. Please see our [Gitcoin grant](https://gitcoin.co/grants/2032/colorado-os) if you'd like to donate to the project development fund.  You can follow us on [Twitter here](https://twitter.com/ColoradoOs). 

### Colorado OS links

General (less technical) primer on Colorado OS - [https://github.com/Colorado-OS/eth-contracts/blob/main/docs/COLORADO_OS_PDF_READMEv2.pdf](https://github.com/Colorado-OS/eth-contracts/blob/main/docs/COLORADO_OS_PDF_READMEv2.pdf)

Ethereum Contracts for Colorado OS - [https://github.com/Colorado-OS/eth-contracts](https://github.com/Colorado-OS/eth-contracts)

NFT & demo UI design - [https://colorado-os.github.io/](https://colorado-os.github.io/)

All Colorado OS code - [https://github.com/Colorado-OS/](https://github.com/Colorado-OS/)

Our ETHDenver 2021 Devfolio page - [https://devfolio.co/submissions/coloradoos-and-grasshopper-f6da](https://devfolio.co/submissions/coloradoos-and-grasshopper-f6da)

ETHDenver 2021 winners announcement - [https://medium.com/ethdenver/ethdenver-coloradojam-2021-bounty-track-quadratic-funding-winners-805cf5f2de76](https://medium.com/ethdenver/ethdenver-coloradojam-2021-bounty-track-quadratic-funding-winners-805cf5f2de76
)

ETHDenver 2021 web site - [https://www.ethdenver.com/ethdenver-2021](https://www.ethdenver.com/ethdenver-2021)

### Colorado OS NFT Art
We're toying with the idea of airdropping some of these NFTs to our [Gitcoin Grant Donors](https://gitcoin.co/grants/2032/colorado-os) after grants round 10 :) 

NFT art and project design was done by the awesome [https://twitter.com/luisblancoach](https://twitter.com/luisblancoach). 

{{< image src="/img/colorado-os-nft-2.png" alt="NFT ART 2" position="center">}}{{< image src="/img/colorado-os-nft-3.png" alt="NFT ART 3" position="center">}}

{{< image src="/img/colorado-os-NFT_SIDE_A.png" alt="NFT ART 4" position="center">}}{{< image src="/img/colorado-os-NFT_SIDE_B.png" alt="NFT ART 5" position="center">}}