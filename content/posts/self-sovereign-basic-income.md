---
title: "Self Sovereign Basic Income Fun[d]"
date: 2021-05-31T04:41:01-06:00
draft: false
toc: false
images: ["/img/420k_base.png", "/img/chart_1.png", "/img/400k_base.png", "/img/chart_2.png"]
tags:
  - ethereum
  - defi
  - self-sovereign-basic-income
  - universal-basic-income
  - colorado-os
---

I set out to test some different DeFi compound interest models today and was thinking about a fund that would behave as a Self Sovereign Basic Income mechanism. Similar to UBI, but without a group or shared pool. 

According to [CNBC](https://finematics.medium.com/lending-and-borrowing-in-defi-explained-aave-compound-28827250ab8) the minimum amount necessary to meet basic needs without dipping into poverty in Colorado in 2018(?) was $27,444 USD. As some time has passed and we don't want to look at the absolute minium required to live, we'll bump that to $40,000 USD per year.  

Using this nice compounding interest calculator from [investor.gov](https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator) we plug in:

```
Initial Investment: 400,000 USD
Length of time in years: 1
Estimated Interest Rate: 10
Interest Rate Variance: 5
``` 

{{< image src="/img/400k_base.png" alt="profit in one year" position="center">}}
{{< image src="/img/chart_1.png" alt="chart for compounding interest on 40k" position="center">}}

10% in DeFi might not be sustainable, but for now we can consider this to be a low estimate for expected rewards on stable coin APY. Also, it helps keep the math more simple :) With 5% variance we miss the minimum 27k though. We don't want to fall below minium required to live so we shoot again. 

With 420K to seed the fund:

```
Initial Investment: 420,000 USD
Length of time in years: 1
Estimated Interest Rate: 10
Interest Rate Variance: 5
``` 

{{< image src="/img/420k_base.png" alt="462k total after one year" position="center">}}
{{< image src="/img/chart_2.png" alt="462k total after one year" position="center">}}

With a self sovereign individual basic income seed of $420,000 USD, @10% APY, and 5% variance, we can see that someone could probably be above the minimum necessary to meet basic needs in Colorado. Moving beyond this simple example we would also consider an anti-inflation switch that would put a portion of the yearly profits back into the fund. Adding to the fund balance manually would be great too of course. 

While 420K might not seem practical for a lot of people, consider the different possible ways someone could feed their SSBI fund. Parents could start funds for kids, fund donations could be solicited, grants programs could be setup to feed individual funds, etc.  I guess it goes full circle back to UBI if we start to think about state funding for SSBI funds.  

**Or, maybe the state could use [Colorado OS](https://colorado-os.github.io/) to implement a wild DeFi sin tax & feed the SSBIs en masse 💥**