---
title: "Introducing Treefund"
date: 2020-02-11T10:11:42-07:00
draft: false
toc: false
images: []
tags:
  - Ethereum
  - Solidity
  - TreeFund
  - blockchain
  - TrustFund
---

In short, TreeFund can be used to facilitate grantors in building and funding a blockchain-based application designed to manage donations for benefactors in creative, simple, and safe ways. Using level one DeFi building blocks, we can build more on the concept.

The original idea stemmed from a desire to share money with family members on-chain. Of course, there is a collective term for this general idea in traditional finance called a trust fund. As it turns out, it's not that hard to create something like this using Ethereum!

Before this project, I had not used Solidity much. Aside from a few days on this excellent [guide to budling ERC721 tokens](https://docs.opensea.io/docs/getting-started) and a quality workshop at [ETHDenver 2019](https://2019.ethdenver.com/) on NFT design, my experience is limited.

That said, the base idea is straightforward, create an Ethereum contract that receives (from a grantor) funds and distributes them to leaf nodes (benefactors). That's it!

Things start to get fun when we consider leaf nodes as wallet contracts and the world of possibilities that can open. As I learn more about the building blocks of open finance, I intend to explore automated positioning, staking and liquidity pools, etc.

You can find the [open-source repository for TreeFund here.](https://gitlab.com/dentino/treefund) The initial feature set is basic by design. I'm not looking for these contracts to end up on [r/defisec](https://reddit.com/r/defisec) anytime soon :)
If this sounds interesting and you'd like to get involved, please reach out to me or make a pull request.
