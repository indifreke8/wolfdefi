---
title: '1 Inch Python Web3 Trading Scripts'
date: 2020-04-18T13:37:33-06:00
draft: false
toc: false
images:
tags:
  - 1Inch
  - DEX
  - ethereum
  - trading
  - web3
  - python
---

A couple of weeks ago, I started working on [my ethereum bugout bot](https://gitlab.com/dentino/ethereum-bugout-bot). One key element of the bot was that it would place trades through decentralized exchanges (DEX). Initially, I wanted to test out [1 Inch Exchange](https://1inch.exchange/#/) or [Loopring](https://loopring.io/trade/ETH-DAI).

I'm not going to dig into what a DEX is on this post, but I will say that you shouldn't assume that DEX's are private OR secure. Log your trades, pay your taxes, and know that smart contracts are not inherently more secure because they are on-chain.

DeFi is still new, and a [sophisticated breed of attacks](https://bankless.substack.com/p/what-happens-when-you-attack-defi-9fe) is emerging that will no doubt cause more havoc soon.

With that out of the way, I found the [1 Inch API](https://1inch.exchange/#/api) documentation to be clear and straight forward. As a result, I just ended up using 1Inch and didn't get a chance to look into Loopring integrations.

1 Inch is a DEX aggregator. When you make a trade through 1 Inch they will go out and try to find the best price across a host of DEXs. They will even allow you to customize the way your order if filled across those exchanges if you want. It's a fascinating concept to see in action on-chain. Innovation like 1 Inch is why I love DeFi.

Initially, I used the 1 Inch API to execute trades. Using the endpoints they [describe here](https://1inch.exchange/#/api), it was easy to achieve an ETH-->DAI exchange. I ran into some issues with going form DAI-->ETH and decided that I'd enjoy the challenge of doing things in a more decentralized way.

In prepping to re-wire the bugout bot to interact with the blockchain directly (instead of APIs), I decided to code up some examples of how you can use Python and Web3 to make 1 Inch trades using contract calls and raw transactions.

> ⮑ You can find [part 1 of the trading guide, 1 Inch DEX using Python & Web3 - Making Contract Calls here](https://wolfdefi.com/projects/1-inch-python-trades/) or skip directly to [repository](https://gitlab.com/dentino/one-inch-trades). Good luck out there 🌵
