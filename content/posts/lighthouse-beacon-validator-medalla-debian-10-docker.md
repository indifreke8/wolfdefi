---
title: 'How to run Lighthouse Beacon Validator on Medalla testnet with Debian 10 & Docker'
date: 2020-08-04T08:05:43-06:00
draft: false
toc: false
images:
tags:
  - medalla
  - ETH2
  - beacon
  - lighthouse
  - docker
  - debian
  - dappnode
---

This is a quick and dirty guide to running Lighthouse beacon node and validator with docker on Debian 10/Buster for the Medalla testnet. I logged the below workflow just before Medalla genesis block was mined and things will probably be changing rapidly. Please do let me know if you have any problems [@wolfdefi](https://twitter.com/wolfdefi)

I'm running this on my Dappnode but it's not using any of the Dappnode native components at this point (I couldn't it get connected to my DappnNode Geth Goerli chain for some reason) so this guide should work for any instance of Debian 10/Buster with or without Dappnode.

This guide assume you've already created validator_keys using the [ETH2 Launchpad](https://medalla.launchpad.ethereum.org/overview). If you haven't done that yet, you'll need to do that first.

### Getting your Goerli GETH node (ETH1) setup

Firstly, an ETH1 node is needed because Eth2 relies upon the Eth1 chain for validator on-boarding. So, all Eth2 validators must have a connection to an Eth1 node. If you don't have an ETH1 node setup you can easily spin one up or you can also skip this step and point your lighthouse_beacon_node at a public node like [https://rpc.goerli.mudit.blog](https://rpc.goerli.mudit.blog). **Your ETH1 node will connect to the Goerli testnet, not mainnet Ethereum :)**

#### Set up local Goerli Geth node with docker

    docker pull ethereum/client-go:stable

    docker network create lighthouse_net

    docker run -d --name "lighthouse_geth" --network="lighthouse_net" -p 30303:30303 -v $HOME/.geth:/root/.ethereum ethereum/client-go --goerli --rpc --rpcaddr "0.0.0.0" --rpcvhosts=*

### Create your lighthouse docker image

Clone the lighthouse repo and build your docker image locally (steps pieced together from various sections of the lighthouse book here: https://lighthouse-book.sigmaprime.io/docker.html)

    git clone https://github.com/sigp/lighthouse.git && cd lighthouse

    docker build . -t lighthouse:local

Test it out:

    docker run lighthouse:local lighthouse --help

Check lighthouse version (there is a bug right now where docker image of lighthouse v-0.2.0 will return 'unknown' for version number fwiw)

    docker run lighthouse:local lighthouse --version

### Running your beacon node

    docker run -d --name "lighthouse_beacon_node" --network="lighthouse_net" -p 9000:9000 -p 127.0.0.1:5052:5052 -v $HOME/.lighthouse:/root/.lighthouse lighthouse:local lighthouse --testnet medalla beacon --graffiti="WolfDeFi" --eth1 --eth1-endpoint="http://lighthouse_geth:8545" --http --http-address 0.0.0.0

- If you're not running a local Gorli Geth node, you can substitute https://rpc.goerli.mudit.blog/ as the --eth-endpoint above

- If you want Proof of Attendance Protocol (https://beaconcha.in/poap)
  get your graffiti tag from this link and use that.

### Import validator keys

You only have to do this one time and it's important to undestand that you should only run one instance of a given validator at the same time! If you try to import the same keys into multiple validators at once, you will be slashed and could lose your ETH. This guide assumes that you've already worked through the [Eth2 LaunchPad for Medalla testnet here](https://medalla.launchpad.ethereum.org/overview).

1. Copy your validator keys into your docker mounted volume

If you're following along with this guide, you will need to copy the validator_keys directory into \$HOME/.lighthouse.

2. Import your validator keys in lighthouse

   docker run -it --rm -v \$HOME/.lighthouse:/root/.lighthouse lighthouse:local lighthouse account validator import --directory=/root/.lighthouse/validator_keys

\*Because we're running the `account validator import` from inside docker we have to reference a location inside container storage, not your local drive!

### Running your validator

    docker run -d --name="lighthouse_validator" --network="lighthouse_net" -v $HOME/.lighthouse:/root/.lighthouse lighthouse:local lighthouse validator --server="http://lighthouse_beacon_node:5052"

At this point, you should have everything up and running but there are a couple things to keep in mind. You'll need your ETH1 client to be completely sync'd up with the Goerli chain before it all works.

### Some notes and final thoughts

You can easily log/monitor your containers with commands like:

    docker logs -f lighthouse_beacon_node
    docker logs -f lighthouse_geth
    docker logs -f lighthouse_validator

You can monitor the status of your validators by entering your public voting keys [https://beaconcha.in/](https://beaconcha.in/)

One way to find your public voting keys is to check your validators directory in your local docker volume:

    ls -lt ~/.lighthouse/validators/

There will be one public key for each of your validators and they will begin with `0x`

You can also see them in your validator logs when the validator starts up initially starts up.

If you run into any networking issues, make sure all three containers are on the docker network `lighthouse_net`

    docker inspect network lighthouse_net

If they aren't all listed for some reason you can manually connect them up like this:

    docker network connect lighthouse_net lighthouse_beacon_node
    docker network connect lighthouse_net lighthouse_geth
    docker network connect lighthouse_net lighthouse_validator

### Upgrading (updated 8/19/2020)

Given the nature of a testnet like Medalla we can expect plenty of updates will be made to the lighthouse client. If an update is required, you should be able to simply pull down the latest lighthouse code and compile your new docker image like so:

    git clone https://github.com/sigp/lighthouse.git && cd lighthouse
    docker build . -t lighthouse:local

From there you will just need to delete the old containers and start the new ones like so:

    docker rm lighthouse_beacon_node
    docker rm lighthouse_validator

    docker run -d --name "lighthouse_beacon_node" --network="lighthouse_net" -p 9000:9000 -p 127.0.0.1:5052:5052 -v $HOME/.lighthouse:/root/.lighthouse lighthouse:local lighthouse --testnet medalla beacon --graffiti="poap8MScysccF1Wtacrzb4og/MqLjrkB" --eth1 --eth1-endpoint="http://lighthouse_geth:8545" --http --http-address 0.0.0.0

    docker run -d --name="lighthouse_validator" --network="lighthouse_net" -v $HOME/.lighthouse:/root/.lighthouse lighthouse:local lighthouse validator --server="http://lighthouse_beacon_node:5052"

This is a beta guide and it's very possible that it contains mistakes. Please keep that in mind! Hit me with my questions, comments, or feedback [@wolfdefi](https://twitter.com/wolfdefi) or the [team in the lighthouse Discord is quite helpful as well](https://discord.gg/cyAszAh) if you have questions. Good luck!
