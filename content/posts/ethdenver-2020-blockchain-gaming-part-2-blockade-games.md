---
title: "ETHDenver 2020 Blockchain Gaming Part 2: Blockade Games"
date: 2020-02-17T10:48:30-07:00
draft: false
toc: false
images: ["/img/fundamental.png", "/img/pineapple.png", "/img/spectrum.png"]
tags:
  - ETHDenver2020
  - Blockchain
  - Gaming
  - Blockade
  - Free-to-play
  - Crypto-puzzles
---

The second ETHDenver2020 gaming talk I attended was Ben Heidorn of Blockade Games, presenting on "The Mainstream Blockchain Game Infrastructure." I was excited about this talk, and it did not disappoint.

Ben set out to walk through his experiences at [Blockage games](https://blockade.games/), bringing blockchain gaming to mainstream audiences with games like [Neon District](https://www.neondistrict.io/), [Plasma Bears](https://plasmabears.com), and [The Pineapple Arcade](https://www.pineapplearcade.net).

In [my first post](https://wolfdefi.com/posts/2020/02/ethdenver-2020-blockchain-gaming-part-1-celerx/) about blockchain gaming at ETHDenver 2020, we looked at the CelerX platform. Blockade Games is quite different from CelerX in their approach, style, and games.

CelerX is an eSports gaming platform in which users can wager crypto or USD in heads up skill-based battles. Blockade Games is focused on free-to-play, free-to-earn games.

One of Ben's key points emerged in their developer thesis:

> Build for the Mainstream User, Decentralize as the technology develops

As you can see by playing any of their fantastic games, the team at Blockade takes this thesis to heart. From an end-user perspective, onboarding is seamless and requires no additional steps related to blockchain. While tokenized in-game items can be purchased and traded, there is no, "KYC-->get wallet->send coins->wrap coins-->Play!" style bottleneck.

Things got especially interesting when Ben dug into The Fundamental Problem.

{{< image src="/img/fundamental.png" alt="Fundamental Problem" position="center" style="border-radius: 8px;" >}}

> It's fine for decentralized, high-security & latency tolerant applications to use blockchain "out-of-the-box," but...
> Mainstream applications and games need immediately consistent available data

In this slide, he draws up the Blockchain Game Spectrum, where one side has games that run on-chain entirely and the other side with only data on-chain.

{{< image src="/img/spectrum.png" alt="Spectrum" position="center" style="border-radius: 8px;" >}}

Ben also discussed the [fallacies of distributed computing](https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing#The_fallacies), which is nice to consider with a discussion about blockchains and mainstream gaming applications.

- The network is reliable
- Latency is zero
- Bandwidth is infinite
- The network is secure
- Topology doesn't change
- There is one administrator
- Transport cost is zero
- The network is homogeneous

Ben went into some details about how they are using the [LoomX Blockchain](https://loomx.io/) with Neon District. In [this talk](https://www.youtube.com/watch?v=NbK5ryWEeAA) from the Blockchain Game Summit, Ben goes into more detail about which blockchain they started with (Ethereum) and why they ended up using LoomX. It's worth the time if you're interested in blockchain gaming.

In researching Blockade Games, I ended up spending some time in the [Pineapple Arcade](https://www.pineapplearcade.net). The premise is rad, but the implementation is unbelievably good.

> The Pineapple Arcade may look like a dream-induced arcade of the 80s, but there's more: the games, the furniture, and even the walls, hide puzzles and scavenger hunts leading to hundreds of thousands of cryptocurrency prizes!

While working through some of the more basic puzzles, I couldn't help but wish I could experience the arcade in a more immersive way. I don't know if that's VR/AR or something more physical like Meow Wolf meets Pineapple Arcade, but I want it!

{{< image src="/img/pineapple.png" alt="Fundamental Problem" width="240" height="180" position="center" style="border-radius: 8px;" >}}

If Blockade continues to succeed in creating seamless on-chain gaming integrations, then we can expect them to be one of the first teams to get a shot at testing the theory that blockchain and gaming are a match made in heaven.

Wrapping things up in style, Ben and [some friends](https://twitter.com/coin_artist/status/1229208102483836929?s=20) received the Judge’s Choice Award for [Die Kitty Die](https://dkd-staging.herokuapp.com/), at the ETHDenver hackathon. The idea started as a way to help you get rid of any old cryptokitties you have sitting around.

## If I understood the premise correctly, they have cleverly gamified NFT destruction into heads up battles!

{{< image src="/img/diekittydie.png" alt="Spectrum" position="center" style="border-radius: 8px;" >}}
