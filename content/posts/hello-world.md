---
title: "Hello Hugo World"
date: 2020-02-11T05:51:14-07:00
draft: false
toc: false
images:
tags:
  - Hugo
  - DeFi
  - ETHDenver
---

I took a quick break from my mission to devour [Solidity](https://solidity.readthedocs.io/) to build this website in prep for ETHDenver. This site is my first go at building with Hugo, and I'm quickly falling in love with the simplicity and forward-thinking design patterns.

I had bookmarked [this site](https://habd.as/) after a recent HN post on living [SIM free](https://habd.as/post/living-without-sim-card/). Inspired by the theme there, I ended up testing out Hugo. It took me less than a day to get this site live with a most excellent (free) pipeline using Gitlab and Cloudflare.

I'll probably stick with this setup for now, but my next goal is to host the site with IPFS.

That said, the speaker lineup for ETHDenver 2020 looks fantastic, and I am excited! I'll probably table Hugo and the move to IPFS for a bit while I continue to prep for this week's events. Hopefully, I will see you there!
